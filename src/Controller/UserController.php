<?php

namespace App\Controller;

use App\Entity\Advertisement;
use App\Entity\Category;
use App\Form\AdvertisementType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \App\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;

class UserController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/my-ads", name="app_my_ads", methods="GET")
     */
    public function index():Response
    {
        $user = $this->getUser();
        $myAds = $this->em->getRepository(Advertisement::class)->findBy(['user' => $user->getId()]);


        return $this->render('user/index.html.twig', ['myAds'=>$myAds]);
    }


}