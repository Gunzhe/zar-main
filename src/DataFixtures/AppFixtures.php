<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Factory\AdvertisementFactory;
use App\Factory\CategoryFactory;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        UserFactory::createMany(5);
        CategoryFactory::createMany(8);
        AdvertisementFactory::createMany(20, ['user'=>UserFactory::random(), 'category'=>CategoryFactory::random()]);




        $manager->flush();
    }
}
