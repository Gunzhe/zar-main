<?php

namespace App\Controller;

use App\Entity\Advertisement;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home_homepage")
     */
    public function homepage(EntityManagerInterface $entityManager):\Symfony\Component\HttpFoundation\Response
    {
        $repository = $entityManager->getRepository(Advertisement::class);
        $advertisement = $repository->findAll();

        return $this->render('home/homepage.html.twig', ['advertisement'=>$advertisement]);
    }

}