<?php

namespace App\Controller;

use App\Entity\Advertisement;
use App\Entity\Category;
use App\Entity\User;
use App\Form\AdvertisementType;
use App\Repository\AdvertisementRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdvertisementController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     * @Route("/ad/create", "app_ad_create")
     */
    public function new(Request $request, ManagerRegistry $doctrine): Response
    {
        $advertisement = new Advertisement();

        $entityManager = $doctrine->getManager();

        $form = $this->createForm(AdvertisementType::class, $advertisement);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $advertisement = $form->getData();

            $entityManager->persist($advertisement);

            $entityManager->flush();

            // ... perform some action, such as saving the task to the database

            return $this->redirectToRoute('app_advertisement_index' );
        }

        return $this->renderForm('ad/create.html.twig', [
            'advertisement' => $form
        ]);

    }

    /**
     * @Route ("/ad/index", "app_ad_index")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Advertisement::class);
        $advertisement = $repository->findAll();

        return $this->render('ad/index.html.twig', ['advertisement' => $advertisement]);

    }

    /**
     * @Route ("/ad/show/{id}", "app_ad_show")
     */
    public function show(EntityManagerInterface $entityManager, int $id): Response
    {
        $advertisement = $entityManager->getRepository(Advertisement::class)->find($id);

        return $this->render('ad/show.html.twig', ['advertisement'=>$advertisement]);
    }

    /**
     * @Route ("/ad/category/{id}", "app_category")
     */
    public function category(Category $category):Response
    {
        $advertisement = $category->getAdvertisements();

        foreach ($advertisement as $ad)
        {
            dump($ad);
        }

        return $this->render('ad/index.html.twig', ['advertisement'=>$advertisement]);
    }

    /**
     * @Route("/ad/edit/{id}", "app_edit")
     */
    public function edit(Request $request, EntityManagerInterface $entityManager, $id): Response
    {
        $repository = $entityManager->getRepository(Advertisement::class);
        $advertisement = $repository->find($id);

        $form = $this->createForm(AdvertisementType::class, $advertisement);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated

            $entityManager->flush();

            // ... perform some action, such as saving the task to the database

            return $this->redirectToRoute('app_advertisement_index');
        }

        return $this->renderForm('ad/edit.html.twig', [
            'form' => $form ,
            'advertisement' => $advertisement,

        ]);
    }

    /**
     * @Route ("/ad/delete/{id}", name="app_delete")
     */
    public function delete(Request $request, EntityManagerInterface $entityManager, $id) :Response
    {
        $repository = $entityManager->getRepository(Advertisement::class);
        $advertisement = $repository->find($id);

        $entityManager->remove($advertisement);
        $entityManager->flush();

        return $this->redirectToRoute('app_advertisement_index');
    }
}